import Vue from 'vue'
import App from './App.vue'

import jQuery from 'jquery'
import 'jquery-easing'
import 'bootstrap'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate from 'vee-validate'
import axios from 'axios'

window.$ = jQuery

Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.use(BootstrapVue)
Vue.use(VeeValidate)

import SmoothScrollLink from './components/SmoothScrollLink.vue'
import Divider from './components/Divider.vue'

Vue.component('smooth-scroll', SmoothScrollLink)
Vue.component('divider-line', Divider)

new Vue({
    render: h => h(App)
}).$mount('#app')
