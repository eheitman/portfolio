<?php

$errors = [];

$args = [
    'name' => FILTER_SANITIZE_STRING,
    'email' => FILTER_SANITIZE_EMAIL,
    'phone' => FILTER_SANITIZE_STRING,
    'message' => [
        'filter' => FILTER_SANITIZE_STRING,
        'flags' => FILTER_FLAG_ENCODE_HIGH
    ]
];

$filtered = filter_input_array(INPUT_POST, $args);

if (! is_array($filtered)) {
    $errors['form'] = 'An error occurred and the email was not sent.';
    http_response_code(400);
    echo json_encode(['success' => false, 'errors' => $errors]);
    exit();
}

if (! filter_var($filtered['email'], FILTER_VALIDATE_EMAIL)) {
    $errors['email'] = $filtered['email'] . ' is an invalid email address.';
    http_response_code(400);
    echo json_encode(['success' => false, 'errors' => $errors]);
    exit();
}

(new Logger(__DIR__ . '/contact.log'))->put(array_values($filtered));

$mailer = new Mailer($filtered['name'], $filtered['email'], $filtered['phone'], $filtered['message']);

if (! $mailer->send()) {
    $errors['form'] = 'An error occurred and the email was not sent.';
    http_response_code(500);
    echo json_encode(['success' => false, 'errors' => $errors]);
    exit();
}

http_response_code(200);
echo json_encode(['success' => true]);

class Mailer
{
    private static $to = 'ethan.t.heitman@gmail.com';

    private $from;

    private $subject;

    private $body;

    public function __construct($name, $email, $phone, $message)
    {
        $this->from = $email;
        $this->subject = 'ethanheitman.com Contact Form : ' . $name;
        $this->body = $this->buildBody($name, $email, $phone, $message);
    }

    public function send() : bool
    {
        return mail(self::$to, $this->subject, $this->body, $this->getHeaders());
    }

    private function buildBody($name, $email, $phone, $message) : string
    {
        $format = "You have received a new message from ethanheitman.com.\n\n";
        $format .= "Name: %s\n\n";
        $format .= "Email: %s\n\n";
        $format .= "Phone: %s\n\n";
        $format .= "Message: %s\n\n";

        return sprintf($format, $name, $email, $phone, $message);
    }

    private function getHeaders() : string
    {
        return 'From: noreply@ethanheitman.com' . "\r\n" .
            'Reply-To: ' . $this->from  . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
    }
}

class Logger
{
    private $filename;

    public function __construct(string $filename)
    {
        try {
            $this->filename = $filename;
            touch($this->filename);
        } catch (Exception $e) {}
    }

    public function put(array $vals)
    {
        if (! file_exists($this->filename)) {
            return;
        }

        $fh = fopen($this->filename, 'a');

        if ($fh === false) {
            return;
        }

        fputcsv($fh, $vals);

        fclose($fh);
    }
}
